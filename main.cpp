#include <vector>

#include <fstream>

#include <stdio.h>

#include "Matrix.h"

#include <stdlib.h>

#include <ctype.h>

#include <iostream>

#include <string.h>

using namespace std;

int print_help_and_exit(char ** argv) { //Prints error and exits program     
  printf("Usage: %s <number 1> <number 2>\n", argv[0]);
  exit(-1);
}

int valid_operation(char ** prog_name, string input) {

  if (input == "x")
    return 0;

  if (input == "X")
    return 0;

  if (input == "+")
    return 0;

  printf("ERROR: incorrect operation ");
  exit(-1);

}

int main(int argc, char ** argv) {

  if (argc != 5) {
    printf("ERROR: incorrect number of arguments ");
    print_help_and_exit(argv);
  }

  string operation = argv[2];
  valid_operation(argv, operation);
  char * output = argv[4];
  char * scalar = argv[3];

  Matrix * matrix_1 = new Matrix();
  if ((operation == "+") && (matrix_1 -> fill_from_file(argv[1]) != -1) && (isdigit(scalar[0]) != 0)) {
    matrix_1 -> add(atoi(argv[3]));
    matrix_1 -> store_in_file(output);
  } else if ((operation == "+") && (matrix_1 -> fill_from_file(argv[1]) != 1)) {
    Matrix * matrix_2 = new Matrix();
    if ((matrix_2 -> fill_from_file(argv[3]) != -1) && (matrix_1 -> add(matrix_2) != -1)) {
      matrix_1 -> store_in_file(output);
      delete matrix_2;
    } else {
      print_help_and_exit(argv);
    }
  } else if ((operation == "X" || "x") && (matrix_1 -> fill_from_file(argv[1]) != -1) && (isdigit(scalar[0]) != 0)) {
    matrix_1 -> mult(atoi(argv[3]));
    matrix_1 -> store_in_file(output);
  } else if ((operation == "X" || "x") && (matrix_1 -> fill_from_file(argv[1]) != -1)) {
    Matrix * matrix_2 = new Matrix();
    if ((matrix_2 -> fill_from_file(argv[3]) != -1) && (matrix_1 -> mult(matrix_2) != -1)) {
      matrix_1 -> store_in_file(output);
      delete matrix_2;
    } else {
      print_help_and_exit(argv);
    }
  } else {
    print_help_and_exit(argv);
  }

  delete matrix_1;

}