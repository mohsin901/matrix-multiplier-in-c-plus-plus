#include <vector>

#include <fstream>

#include <stdio.h>

#include "Matrix.h"

#include <stdlib.h>

#include <ctype.h>

#include <iostream>

#include <string.h>

#include <cstdlib>

#include <sstream>

using namespace std;

Matrix::Matrix() {

}

int Matrix::rows() {
  int mat_rows = this -> _matrix.size();
  return mat_rows;
}

int Matrix::columns() {
  int mat_cols = this -> _matrix[0].size();
  return mat_cols;
}

int Matrix::mult(int val) {
  for (int i = 0; i < this -> _matrix.size(); ++i) {
    for (int j = 0; j < this -> _matrix[0].size(); ++j) {
      this -> _matrix[i][j] = this -> _matrix[i][j] * val;
    }
  }
  return 0;
}

int Matrix::store_in_file(char * path) {
  ofstream dest(path);
  if (dest.is_open()) {
    for (int n = 0; n < this -> _matrix.size(); n++) {
      for (int m = 0; m < this -> _matrix[0].size(); m++) {
        if (m == columns() - 1) {
          dest << index(n, m);
        } else {
          dest << index(n, m) << " ";
        }

      }
      dest << endl;
    }
    dest.close();
    return 0;
  } else {
    return -1;
  }
}

int Matrix::add(int val) {
  for (int i = 0; i < _matrix.size(); ++i) {
    for (int j = 0; j < _matrix[0].size(); ++j) {
      this -> _matrix[i][j] = _matrix[i][j] + val;
    }
  }
  return 0;
}

void Matrix::print() {
  for (const std::vector < int > & v: this -> _matrix) {
    for (int x: v) std::cout << x << ' ';
    std::cout << std::endl;
  }
}

int Matrix::index(int row, int col) {
  return this -> _matrix[row][col];
}

int Matrix::mult(Matrix * m) {

  if (this -> _matrix[0].size() != m -> _matrix.size()) {
    return -1;
  }

  Matrix * mat_result = new Matrix();

  for (int i = 0; i < this -> _matrix.size(); i++) {
    vector < int > temp;
    for (int j = 0; j < m -> _matrix[0].size(); j++) {
      temp.push_back(0);

    }
    mat_result -> _matrix.push_back(temp);
  }

  for (int i = 0; i < this -> _matrix.size(); ++i) {
    for (int j = 0; j < m -> _matrix[0].size(); ++j) {
      for (int k = 0; k < this -> _matrix[0].size(); ++k) {
        mat_result -> _matrix[i][j] += this -> _matrix[i][k] * m -> _matrix[k][j];
      }

    }

  }
  this -> _matrix = mat_result -> _matrix;

  return 0;
}

int Matrix::add(Matrix * m) {

  if (this -> _matrix.size() != m -> _matrix.size()) {
    return -1;
  }

  if (this -> _matrix[0].size() != m -> _matrix[0].size()) {
    return -1;
  }
  for (int i = 0; i < this -> _matrix.size(); ++i) {
    for (int j = 0; j < this -> _matrix[0].size(); ++j) {
      this -> _matrix[i][j] = this -> _matrix[i][j] + m -> _matrix[i][j];
    }
  }
  return 0;
}

int Matrix::fill_from_file(char * path) {
  this -> _matrix.clear();

  ifstream in (path);
  if (! in ) {
    return -1;
  }
  string txt, val;
  int row = 0;
  while (getline( in , txt)) {
    if (txt.size() > 0) {
      this -> _matrix.push_back(vector < int > ());
      stringstream ss(txt);
      while (getline(ss, val, ' ')) {
        this -> _matrix[row].push_back(stoi(val));
      }
      row++;
    }
  } in .close();
  return 0;
}